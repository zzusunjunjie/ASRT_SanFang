# coding: UTF-8
from keras.models import Model
from keras.layers import Input, Conv2D, Lambda, MaxPooling2D, Activation
from keras.layers import Dropout, Dense, Reshape
from keras import backend as K
from keras.optimizers import Adam
import tensorflow as tf
import numpy as np
import os
from random import randint
from general.shareparam import OUTPUT_SIZE, MAX_HMC_COUNT, MAX_STRING_LENGTH, FEATURE_LENGTH


class SpeechModelSF:
    def __init__(self, dataPath):
        self.dataPath = dataPath

    def createCNNLayer(self, input, np_filter, use_bias, rate, maxpool_strides):
        layer = Conv2D(np_filter, kernel_size=(3, 3), activation='relu', use_bias=use_bias, padding='same',
                       kernel_initializer='he_normal')(input)
        layer = Dropout(rate)(layer)
        layer = Conv2D(np_filter, kernel_size=(3, 3), activation='relu', use_bias=use_bias, padding='same',
                       kernel_initializer='he_normal')(layer)
        outLayer = MaxPooling2D(pool_size=maxpool_strides, padding='valid')(layer)
        return outLayer

    def createCNN(self, inputData):
        outPut = sf.createCNNLayer(inputData, 32, False, 0.2, (2, 2))
        outPut = sf.createCNNLayer(outPut, 64, True, 0.2, (2, 2))
        outPut = sf.createCNNLayer(outPut, 128, True, 0.2, (2, 2))
        outPut = sf.createCNNLayer(outPut, 128, True, 0.3, (2, 2))
        outPut = sf.createCNNLayer(outPut, 128, True, 0.3, (1, 1))

        height = int(outPut.shape[2]) * int(outPut.shape[3])
        outPut = Reshape((FEATURE_LENGTH, height))(outPut)
        outPut = Dense(128, activation="relu", use_bias=True, kernel_initializer='he_normal')(outPut)
        outPut = Dropout(0.3)(outPut)
        outPut = Dense(OUTPUT_SIZE, activation="relu", use_bias=True, kernel_initializer='he_normal')(outPut)
        outPut = Activation('softmax', name='activation')(outPut)
        return outPut

    def ctc_lambda_func(self, args):
        predData, labels, inputs_length, labels_length = args
        predData = predData[:, :, :]
        return K.ctc_batch_cost(labels, predData, inputs_length, labels_length)

    def createSFSpeechModel(self):
        inputData = Input(shape=(MAX_HMC_COUNT, FEATURE_LENGTH, 1), name="input_shape")
        predData = self.createCNN(inputData=inputData)
        model_pred = Model(inputs=inputData, outputs=predData)


        labels = Input(name='labels', shape=[MAX_STRING_LENGTH], dtype='float32')
        inputs_length = Input(name='inputs_length', shape=[1], dtype='int64')
        labels_length = Input(name='labels_length', shape=[1], dtype='int64')
        loss_out = Lambda(self.ctc_lambda_func, name='ctc')([predData, labels, inputs_length,
                                                                                labels_length])
        model = Model(inputs=[inputData, labels, inputs_length, labels_length], outputs=loss_out)
        model.summary()

        opt = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, decay=0.0, epsilon=10e-8)
        try:
            model.compile(loss={'ctc': lambda y_true, y_pred: predData}, optimizer=opt)
        except ValueError:
            print("compile存在无效的参数，请检查")
            return None
        else:
            print("模型建立成功")
            return model_pred, model

if __name__ == '__main__':
    sf = SpeechModelSF("")
    sf.createSFSpeechModel()
