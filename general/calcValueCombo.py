# coding: UTF-8
from math import pow
import os


def getAutioTimeAndCombox():
    print("满足条件的组合为:\r\n")
    number = 0
    for x in range(1600, 7200):
        for y in range(200, 400):
            for z in range(3, 5):
                α = pow(2, z)
                if ((x // α) == y and (x % α) == 0) and (y % α) == 0:
                    print("x={0} y={1} z={2}\r\n".format(x, y, z))
                    number += 1
    print("共计{}种组合".format(number))

if __name__ == '__main__':
    getAutioTimeAndCombox()
