# coding: UTF-8

import wave
import matplotlib.pyplot as plt
import numpy as np
from scipy.fftpack import fft
import os
from ffmpy3 import FFmpeg
from general.shareparam import FEATURE_LENGTH


def convertWavFrequency(file_path):
    if os.path.isfile(file_path):
        pathtuple = os.path.split(file_path)
        filename = pathtuple[1].split('.')
        sourceSuffix = filename[1]

        outfilename = pathtuple[0] + "\\" + filename[0] + "sftemp.wav"
        if os.path.isfile(outfilename):
            os.remove(outfilename)

        if sourceSuffix.endswith("wav"):
            FFmpeg(inputs={file_path: None}, outputs={outfilename: "-ar 16000 -ac 1"}).run()
        else:
            FFmpeg(inputs={file_path: None}, outputs={outfilename: "-acodec pcm_s16le -ar 16000 -ac 1"}).run()
        return outfilename


def getAudioFileSignalAndFrequency(file_path):
    with wave.open(file_path, mode="rb") as wav:
        fs = wav.getframerate()
        nchannel = wav.getnchannels()
        str_data = wav.readframes(wav.getnframes())
        wav_data = np.fromstring(str_data, dtype=np.short)
        wav_data.shape = -1, nchannel
        wav_data = wav_data.T
        return fs, wav_data


def getTimeFrequencyGraph(file_path):
    outfilepath = convertWavFrequency(file_path)
    fs, wavsignal = getAudioFileSignalAndFrequency(outfilepath)
    # wav波形加时间窗并平移10ms
    frameshift = 15  # 每次帧移15ms
    time_window = 40  # 单位为ms
    window_length = fs // 1000 * time_window
    x = np.linspace(0, window_length - 1, window_length, dtype=np.int64)
    w = 0.54 - 0.46 * np.cos(2 * np.pi * x / (window_length - 1))

    wav_arr = np.array(wavsignal)
    wav_length = wavsignal.shape[1]
    range_end = int(len(wavsignal[0])/fs * 1000 - time_window) // frameshift + 1
    input_data = np.zeros((range_end, FEATURE_LENGTH), dtype=np.float)
    data_line = np.zeros((1, window_length), dtype=np.float)
    for i in range(0, range_end):
        # 分帧
        p_start = i * (fs//1000 * frameshift)
        p_end = p_start + window_length
        data_line = wav_arr[0, p_start:p_end]
        # 加窗
        data_line = data_line * w
        # 快速傅里叶变换
        data_line = np.abs(fft(data_line)) / wav_length
        input_data[i] = data_line[0:window_length//2]
    # 取对数，求db(分贝)
    input_data = np.log(input_data + 1)
    length = len(input_data)
    return input_data


if __name__ == '__main__':
    data_input = getTimeFrequencyGraph("..\\dataset\\20200107000000.wav")
    plt.imshow(data_input)
    plt.xlabel("Time(sec)", fontsize=10)
    plt.ylabel("Freq(Hz)", fontsize=10)
    plt.show()
