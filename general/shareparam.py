# coding: UTF-8

# 字典中汉字编号 +
OUTPUT_SIZE = 1424
MAX_STRING_LENGTH = 256
# 设汉明窗的长度为X(单位为ms)为40ms，偏移为Y(单位为ms)默认为15ms，
# 音频文件的最大时长计算公式: Y * MAX_HMC_COUNT + (X - Y)
# 识别最大音频时长为: 76.825s 大约等于76s
# 汉明窗数目与特征
MAX_HMC_COUNT = 5120
FEATURE_LENGTH = 320
